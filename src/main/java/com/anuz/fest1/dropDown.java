/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anuz.fest1;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;

/**
 *
 * @author anuz
 */


@ManagedBean
@SessionScoped
public class dropDown implements Serializable{
    
    private Map<String,Map<String,String>> data = new HashMap<String, Map<String,String>>();
    private String faculty;  
    private String program;
    private Map<String,String> faculties;
    private Map<String,String> programs;
    
    @PostConstruct
    public void init(){
        faculties = new HashMap<String,String>();
        faculties.put("Management","Management");
        faculties.put("Science and Techology", "Science and Technology");
        
        Map<String,String> map = new HashMap<String, String>();
        map.put("BBA", "BBA");
        map.put("BBS", "BBS");
        data.put("Management", map);
        
        
        map = new HashMap<String, String>();
        map.put("BE Computer", "BE Computer");
        map.put("BCA", "BCA");
        data.put("Science and Technology", map);
    }
    
     public Map<String, Map<String, String>> getData() {
        return data;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public Map<String, String> getFaculties() {
        return faculties;
    }

//    public void setFaculties(Map<String, String> faculties) {
//        this.faculties = faculties;
//    }

    public Map<String, String> getPrograms() {
        return programs;
    }

//    public void setPrograms(Map<String, String> programs) {
//        this.programs = programs;
//    }
      
     public void onFacultyChange(AjaxBehaviorEvent e){
         
         // it means this method is called
         
         if(faculty != null && !faculty.equals("")){
             programs = data.get(faculty);
         }else{
             programs = new HashMap<String,String>();     
         }
            
     }
}