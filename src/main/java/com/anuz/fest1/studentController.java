/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anuz.fest1;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author anuz
 */
@ManagedBean
@SessionScoped
public class studentController {
    
    
    private Student student;
    private List<Student> students;
    
    
    
    
    
    @PostConstruct
    public void init(){
    
        students= new ArrayList<Student>();
        students.add(new Student("", "", "","",""));
        student=new Student("","","","","");
    
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
    
    
    

    public Student getStudent() {
        return student;
    }
    
    public void saveInfo(){
    
        System.out.println("first name:"+ student.getFname());
        System.out.println("middle name:"+ student.getMname());
        System.out.println("last name:"+ student.getLname());
        System.out.println("faculty:"+student.getFaculty());
        System.out.println("program:"+student.getProgram());
        
        students.add(student);
        student= new Student("", "", "","","");
        
        
    }
    
    
    
}